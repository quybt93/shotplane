using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HomeMenu))]
public class HomeMenuEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("RESET ALL"))
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("DELETED ALL DATA");
        }

    }
}
