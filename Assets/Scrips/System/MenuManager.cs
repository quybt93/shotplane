using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance;
    public GameObject uI, gameOver, finish, LoadingUI;
    public Text txtLevels;
    public Text txtScores;
    public int Point { get; set; }

    private void Awake()
    {
        Instance = this;
        EnemyController.onDeath += AddPoint;
    }
    private void Update()
    {
        if (LevelController.instance.finished)
            Finish();
    }
    void Start()
    {
        uI.SetActive(true);
        finish.SetActive(false);
        gameOver.SetActive(false);
        LoadingUI.SetActive(false);
        txtLevels.text  = "Level " + GlobalValue.LevelPlaying;
        txtScores.text = Point.ToString();
        

        if (Time.timeScale == 0)
            Time.timeScale = 1;
    }

    public void Play()
    {
        uI.SetActive(true);
    }
    public void Finish()
    {
        if (GlobalValue.LevelPlaying >= GlobalValue.LevelHighest)
        {
            GlobalValue.LevelHighest++;
        }
        Invoke("FinishCo", 1);
    }

    void FinishCo()
    {

        uI.SetActive(false);
        finish.SetActive(true);
    }

    public void GameOver()
    {
        StopAllCoroutines();
        Invoke("GameOverCo", 0);
    }

    void GameOverCo()
    {
        uI.SetActive(false);
        gameOver.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void NextLevel()
    {
        GlobalValue.LevelPlaying++;
        LoadingUI.SetActive(true);
        SceneManager.LoadSceneAsync("Level " + GlobalValue.LevelPlaying);
    }

    public void Home()
    {
        GlobalValue.LevelPlaying = -1;
        LoadingUI.SetActive(true);
        SceneManager.LoadSceneAsync("MainMenu");
    }
    public void AddPoint(EnemyController enemy)
    {
        Point += 1;
        txtScores.text = Point.ToString();
    }
    private void OnDisable()
    {
        EnemyController.onDeath -= AddPoint;
    }
}
