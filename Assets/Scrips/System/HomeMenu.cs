using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeMenu : MonoBehaviour
{
    public static HomeMenu Instance;

    public GameObject LoadingUI;

    public void Awake()
    {
        Instance = this;
        LoadingUI.SetActive(false);

        Time.timeScale = 1;
    }
    public void LoadScene()
    {
        GlobalValue.LevelPlaying = GlobalValue.LevelHighest;
        LoadingUI.SetActive(true);
        SceneManager.LoadSceneAsync("Level " + GlobalValue.LevelPlaying);
    }
}
