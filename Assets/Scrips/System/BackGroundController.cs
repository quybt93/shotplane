using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundController : MonoBehaviour
{
    public Transform target;
    public float speed;
    Renderer bg;
    void Start()
    {
        if (target == null)
            target = Camera.main.transform;
        bg = GetComponent<Renderer>();
    }
    void Update()
    {
        var offset = (speed * Time.time) % 1;
        bg.material.mainTextureOffset = new Vector2(bg.material.mainTextureOffset.x, offset);
    }
}
