using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalValue : MonoBehaviour
{
    static int levelPlaying = -1;
    public static int LevelPlaying
    {
        get { return levelPlaying; }
        set
        {
            int newValue = Mathf.Clamp(value, -1, LevelHighest);
            levelPlaying = newValue;
        }
    }
    public static int LevelHighest
    {
        get { return PlayerPrefs.GetInt("LevelHighest", 1); }
        set
        {
            int newValue = Mathf.Clamp(value, 1, 3);
            PlayerPrefs.SetInt("LevelHighest", newValue);
        }
    }
}
