﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float Speed = 10;
    public int Damage = 1;
    public LayerMask LayerCollision;
    public GameObject destroyEffect;
    Vector2 lastPosition;
    public float timeToLive = 3;
    float timeLiveCounter = 0;
    
    private void OnEnable()
    {
        lastPosition = transform.position;
        timeLiveCounter = timeToLive;
    }
    public void Update()
    {
        if ((timeLiveCounter -= Time.deltaTime) <= 0)
        {
            DestroyProjectile();
            return;
        }

        transform.position += transform.up * Speed * Time.deltaTime;

        var hits = Physics2D.LinecastAll(lastPosition, transform.position, LayerCollision);
        if (hits.Length > 0)
        {
            ContactTarget(hits);
        }
    }
    public void LateUpdate()
    {
        lastPosition = transform.position;
    }
    void ContactTarget(RaycastHit2D[] hits)
    {
        foreach (var hit in hits)
        {
            var takeDamage = (ICanTakeDamage)hit.collider.gameObject.GetComponent(typeof(ICanTakeDamage));
            if (takeDamage != null)
            {
                DestroyProjectile();
                takeDamage.TakeDamage(Damage);
            }
        }
    }
    void DestroyProjectile()
    {
        if (destroyEffect != null)
            SpawnSystemHelper.GetNextObject(destroyEffect, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }
}


