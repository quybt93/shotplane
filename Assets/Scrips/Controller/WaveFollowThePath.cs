﻿using System.Collections;
using UnityEngine;
using System;

public enum MovingType
{
    None, cubicHermite
}

public class WaveFollowThePath : WaveController
{
    public Transform[] pathPoints;
    public bool rotationByPath;
    public MovingType movingType;
    public float timeBetween;
    public float waitTime = 0f;

    private void FixedUpdate()
    {
        if (isActivated)
        {
            StartCoroutine(CreateEnemyWave());
            isActivated = false;
        }
    }
    IEnumerator CreateEnemyWave()
    {
        for (int i = 0; i < count; i++) 
        {
            GameObject newEnemy = SpawnEnemy(shooting, transform.position, false);
            FollowThePath followComponent = newEnemy.AddComponent<FollowThePath>(); 
            followComponent.path = pathPoints;         
            followComponent.speed = speed;        
            followComponent.rotationByPath = rotationByPath;
            followComponent.loop = loop;
            followComponent.movingType = movingType;
            followComponent.waitTime = waitTime;
            followComponent.SetPath();
            
            yield return new WaitForSeconds(timeBetween); 
        }
        isSpawnFinished = true;
    }

    void OnDrawGizmos()  
    {
        Gizmos.color = Color.red;
        foreach (Transform pathPoint in pathPoints)
        {
            Gizmos.DrawSphere(pathPoint.position, 0.2f);
        }
        DrawPath(pathPoints);
    }
    void DrawPath(Transform[] path)
    {
        Vector3[] pathPositions = new Vector3[path.Length];
        for (int i = 0; i < path.Length; i++)
        {
            pathPositions[i] = path[i].position;
        }
        Vector3[] newPathPositions = CreatePoints(pathPositions);
        Vector3 previosPositions;
        if (movingType == MovingType.cubicHermite)
        {
            previosPositions = Interpolate(newPathPositions, 0);
        }
        else
        {
            previosPositions = Linear(pathPositions, 0);
        }
        Gizmos.color = Color.red;
        int SmoothAmount = path.Length * 20;
        for (int i = 1; i <= SmoothAmount; i++)
        {
            float t = (float)i / SmoothAmount;
            Vector3 currentPositions;
            if (movingType == MovingType.cubicHermite)
            {
                currentPositions = Interpolate(newPathPositions, t);
            }
            else
            {
                currentPositions = Linear(pathPositions, t);
            }
            Gizmos.DrawLine(currentPositions, previosPositions);
            previosPositions = currentPositions;
        }
    }
    Vector3 Linear(Vector3[] path, float t)
    {
        int numSections = path.Length - 1;
        int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
        float u = t * numSections - currPt;

        Vector3 start = path[currPt];
        Vector3 end = path[currPt + 1];

        return Vector3.Lerp(start, end, u);
    }
    Vector3 Interpolate(Vector3[] path, float t) 
    {
        int numSections = path.Length - 3;
        int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
        float u = t * numSections - currPt;
        Vector3 a = path[currPt];
        Vector3 b = path[currPt + 1];
        Vector3 c = path[currPt + 2];
        Vector3 d = path[currPt + 3];
        return 0.5f * ((-a + 3f * b - 3f * c + d) * (u * u * u) + (2f * a - 5f * b + 4f * c - d) * (u * u) + (-a + c) * u + 2f * b);
    }

    Vector3[] CreatePoints(Vector3[] path)
    {
        Vector3[] pathPositions;
        Vector3[] newPathPos;
        int dist = 2;
        pathPositions = path;
        newPathPos = new Vector3[pathPositions.Length + dist];
        Array.Copy(pathPositions, 0, newPathPos, 1, pathPositions.Length);
        newPathPos[0] = newPathPos[1] + (newPathPos[1] - newPathPos[2]);
        newPathPos[newPathPos.Length - 1] = newPathPos[newPathPos.Length - 2] + (newPathPos[newPathPos.Length - 2] - newPathPos[newPathPos.Length - 3]);
        if (newPathPos[1] == newPathPos[newPathPos.Length - 2])
        {
            Vector3[] LoopSpline = new Vector3[newPathPos.Length];
            Array.Copy(newPathPos, LoopSpline, newPathPos.Length);
            LoopSpline[0] = LoopSpline[LoopSpline.Length - 3];
            LoopSpline[LoopSpline.Length - 1] = LoopSpline[2];
            newPathPos = new Vector3[LoopSpline.Length];
            Array.Copy(LoopSpline, newPathPos, LoopSpline.Length);
        }
        return (newPathPos);
    }
}
