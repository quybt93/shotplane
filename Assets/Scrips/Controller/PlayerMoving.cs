using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Borders
{
    public float minXOffset = 0.5f, maxXOffset = 0.5f, minYOffset = 0.5f, maxYOffset = 0.5f;
    [HideInInspector] public float minX, maxX, minY, maxY;
}

public class PlayerMoving : MonoBehaviour
{
    public Borders borders;
    Camera mainCamera;
    bool controlIsActive = true;
    public float speed = 30f;
    Vector3 offset;

    private void Start()
    {
        mainCamera = Camera.main;
        ResizeBorders(); 
    }

    private void Update()
    {
        if (controlIsActive)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                mousePosition.z = transform.position.z;
                offset = transform.position - mousePosition;
            }

            if (Input.GetMouseButton(0))      
            {
                Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                mousePosition.z = transform.position.z;

                transform.position = Vector3.MoveTowards(transform.position, mousePosition + offset, speed * Time.deltaTime);
            }
            transform.position = new Vector3  
                (
                Mathf.Clamp(transform.position.x, borders.minX, borders.maxX),
                Mathf.Clamp(transform.position.y, borders.minY, borders.maxY),
                0
                );
        }
    }

    void ResizeBorders()
    {
        borders.minX = mainCamera.ViewportToWorldPoint(Vector2.zero).x + borders.minXOffset;
        borders.minY = mainCamera.ViewportToWorldPoint(Vector2.zero).y + borders.minYOffset;
        borders.maxX = mainCamera.ViewportToWorldPoint(Vector2.right).x - borders.maxXOffset;
        borders.maxY = mainCamera.ViewportToWorldPoint(Vector2.up).y - borders.maxYOffset;
    }
}
