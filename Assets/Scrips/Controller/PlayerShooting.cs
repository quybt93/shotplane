﻿using UnityEngine;


public class PlayerShooting : MonoBehaviour 
{
    public float asdp = 2f;
    public int bulletNumber = 3;
    public float offsetAngle = 10f;
    float shotTime;
    public GameObject bulletPrefab;
    public Transform shootingPoint;
    bool shootingIsActive = true; 
    public static PlayerShooting instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Update()
    {
        if (shootingIsActive)
        {
            if (Time.time > shotTime)
            {
                Shoot();
                shotTime = Time.time + 1 / asdp;
            }
        }
    }
    void Shoot()
    {
        float j = (float)(bulletNumber - 1) / 2;
        for (int i = 0; i < bulletNumber; i++)
        {
            SpawnSystemHelper.GetNextObject(bulletPrefab, shootingPoint.position, Quaternion.Euler(0, 0, j * offsetAngle));
            j--;
        }
    }
}
