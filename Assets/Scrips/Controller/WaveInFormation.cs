using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class WaveInFormation : WaveController
{
    public Squad startSquad;
    public Squad[] formations;
    public float delay = 5f;
    
    private void FixedUpdate()
    {
        if (isActivated)
        {
            CreateEnemyWave(startSquad);
            StartCoroutine(ActionCo());
            isActivated = false;
        }
    }
    void CreateEnemyWave(Squad formation)
    {
        List<Vector3> listPos = new List<Vector3>(); 
        foreach (Transform child in formation.squad)
        {
            listPos.Add(child.position);
        }
        if (listPos.Count == 0)
        {
            listPos.Add(formation.squad.position);
        }
        for (int i = 0; i < count; i++)
        {
            SpawnEnemy(shooting, listPos[i % listPos.Count], true);
        }
        isSpawnFinished = true;
    }
    void ChangeFormation(Squad formation, bool _disImmortal = false)
    {
        List<Vector3> listPos = new List<Vector3>();
        foreach (Transform child in formation.squad)
        {
            listPos.Add(child.position);
        }
        if (listPos.Count == 0)
        {
            listPos.Add(formation.squad.position);
        }
        for (int i = 0; i < enemies.Count; i++)
        {
            StartCoroutine(enemies[i].MoveToPoint(listPos[i % listPos.Count], formation.timeMove, _disImmortal));
        }
    }
    IEnumerator ActionCo()
    {
        do
        {
            for (int i = 0; i < formations.Length; i++)
            {
                yield return new WaitForSeconds(delay);
                ChangeFormation(formations[i], i == formations.Length - 1 ? true : false);
            }
        } while (loop);
    }
}
