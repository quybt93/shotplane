﻿using System.Collections;
using UnityEngine;

public class LevelController : MonoBehaviour 
{
    public static LevelController instance;
    WaveController[] enemyWaves;
    [HideInInspector] public bool finished;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        enemyWaves = FindObjectsOfType<WaveController>();
    }
    private void Start()
    {
        StartCoroutine(CreateEnemyWaveCo());
    }
    bool temp;
    private void Update()
    {
        temp = true;
        for (int i = 0; i < enemyWaves.Length; i++)
        {
            if (!enemyWaves[i].isWaveFinished)
            {
                temp = false;
                break;
            }
        }
        if (temp) { finished = true; }
    }
    IEnumerator CreateEnemyWaveCo()
    {
        for (int i = 0; i < enemyWaves.Length; i++)
        {
            if (PlayerController.instance != null)
                Create(enemyWaves[i]);
            while (!enemyWaves[i].isWaveFinished)
            {
                yield return null;
            }
        }
    }
    void Create(WaveController wave)
    {
        wave.isActivated = true;
    }
}
