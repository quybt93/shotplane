using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Squad", menuName = "Squad")]
public class Squad : ScriptableObject
{
    public Transform squad;
    public float timeMove = 2f;
}
