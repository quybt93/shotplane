using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, ICanTakeDamage
{
    public int maxHealth = 3;
    public int currentHealth { get; private set; }
    public static event Action<int> healthChangeEvent;
    public GameObject destructionFX;
    public GameObject hitEffect;

    public static PlayerController instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        healthChangeEvent = null;
    }
    void Start()
    {
        currentHealth = maxHealth;
    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (hitEffect)
        {
            SpawnSystemHelper.GetNextObject(hitEffect, transform.position, Quaternion.identity);
        }
        healthChangeEvent(currentHealth);
        if (currentHealth <= 0)
            Destruction();
    }
    void Destruction()
    {
        if (destructionFX)
            SpawnSystemHelper.GetNextObject(destructionFX, transform.position, Quaternion.identity);
        gameObject.SetActive(false);
        MenuManager.Instance.GameOver();
    }
}
