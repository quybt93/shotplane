﻿using UnityEngine;
using System;


public class FollowThePath : MonoBehaviour {
        
    [HideInInspector] public Transform [] path; 
    [HideInInspector] public float speed; 
    [HideInInspector] public bool rotationByPath; 
    [HideInInspector] public bool loop;
    [HideInInspector] public MovingType movingType;
    [HideInInspector] public float waitTime = 0f;
    int fromWaypointIndex;
    float percentBetweenWaypoints;
    float nextMoveTime;
    float currentPathPercent; 
    Vector3[] pathPositions; 
    [HideInInspector] public bool movingIsActive;
    public void SetPath() 
    {
        currentPathPercent = 0;
        pathPositions = new Vector3[path.Length];
        for (int i = 0; i < pathPositions.Length; i++)
        {
            pathPositions[i] = path[i].position;
        }
        
        transform.position = NewPositionByPath(pathPositions, 0); 
        if (!rotationByPath)
            transform.rotation = Quaternion.identity;
        movingIsActive = true;
    }
    
    private void Update()
    {
        if (movingIsActive)
        {
            if (Time.time <= nextMoveTime)
            {
                return;
            }

            fromWaypointIndex %= pathPositions.Length;
            percentBetweenWaypoints += Time.deltaTime * speed / 100 * (pathPositions.Length -1);
            percentBetweenWaypoints = Mathf.Clamp01(percentBetweenWaypoints);

            currentPathPercent += speed / 100 * Time.deltaTime;

            transform.position = NewPositionByPath(pathPositions, currentPathPercent);
            if (rotationByPath)    
            {
                transform.up = Interpolate(CreatePoints(pathPositions), currentPathPercent + 0.01f) - transform.position;
                transform.Rotate(Vector3.forward * 90);
            }
            if (percentBetweenWaypoints >= 1)
            {
                percentBetweenWaypoints = 0;
                fromWaypointIndex++;
                if (fromWaypointIndex >= pathPositions.Length - 1)
                {
                    if (!loop)
                    {
                        EnemyController enemyComponent = gameObject.GetComponent<EnemyController>();
                        if (enemyComponent != null)
                        {
                            enemyComponent.Destruction();
                        }
                        else
                        {
                            Destroy(gameObject);
                        }
                    }
                    currentPathPercent = 0;
                    fromWaypointIndex = 0;
                }
                if (waitTime != 0)
                {
                    nextMoveTime = Time.time + waitTime;
                }
            }
        }
    }

    Vector3 NewPositionByPath(Vector3 [] pathPos, float percent) 
    {
        if (movingType == MovingType.cubicHermite)
        {
            return Interpolate(CreatePoints(pathPos), percent);
        }
        else
        {
            return Linear(pathPos, percent);
        }
    }
    Vector3 Linear(Vector3[] path, float t)
    {
        int numSections = path.Length - 1;
        int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
        float u = t * numSections - currPt;

        Vector3 start = path[currPt];
        Vector3 end = path[currPt + 1];

        return Vector3.Lerp(start, end, u);
    }

    Vector3 Interpolate(Vector3[] path, float t) 
    {
        int numSections = path.Length - 3;
        int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
        float u = t * numSections - currPt;
        Vector3 a = path[currPt];
        Vector3 b = path[currPt + 1];
        Vector3 c = path[currPt + 2];
        Vector3 d = path[currPt + 3];
        return 0.5f * ((-a + 3f * b - 3f * c + d) * (u * u * u) + (2f * a - 5f * b + 4f * c - d) * (u * u) + (-a + c) * u + 2f * b);
    }

    Vector3[] CreatePoints(Vector3[] path) 
    {
        Vector3[] pathPositions;
        Vector3[] newPathPos;
        int dist = 2;
        pathPositions = path;
        newPathPos = new Vector3[pathPositions.Length + dist];
        Array.Copy(pathPositions, 0, newPathPos, 1, pathPositions.Length);
        newPathPos[0] = newPathPos[1] + (newPathPos[1] - newPathPos[2]);
        newPathPos[newPathPos.Length - 1] = newPathPos[newPathPos.Length - 2] + (newPathPos[newPathPos.Length - 2] - newPathPos[newPathPos.Length - 3]);
        if (newPathPos[1] == newPathPos[newPathPos.Length - 2])
        {
            Vector3[] LoopSpline = new Vector3[newPathPos.Length];
            Array.Copy(newPathPos, LoopSpline, newPathPos.Length);
            LoopSpline[0] = LoopSpline[LoopSpline.Length - 3];
            LoopSpline[LoopSpline.Length - 1] = LoopSpline[2];
            newPathPos = new Vector3[LoopSpline.Length];
            Array.Copy(LoopSpline, newPathPos, LoopSpline.Length);
        }

        return (newPathPos);
    }
}
