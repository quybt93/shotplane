﻿using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

[System.Serializable]
public class Shooting
{
    public float shotTimeMin = 2f;
    public float shotTimeMax = 5f;
}
public class EnemyController : MonoBehaviour, ICanTakeDamage
{
    public int maxHealth = 3;
    [SerializeField] public bool isImmortal;
    public static event Action<EnemyController> onDeath;
    public int currentHealth { get; private set; }
    public Transform shootingPoint;
    public GameObject bulletPrefab;
    public GameObject destructionVFX;
    public GameObject hitEffect;

    public float shotTimeMin = 2f;
    public float shotTimeMax = 5f;
    //[HideInInspector] public bool useTrack = false;
    [HideInInspector] public Transform target;
    private void OnEnable()
    {
        currentHealth = maxHealth;
        StartCoroutine(ShootRandomly());
    }
    //private void Update()
    //{
    //    if (useTrack)
    //    {
    //        transform.position = target.position;
    //    }
    //}
    void ActivateShooting()
    {
        if (isImmortal)
            return;
        SpawnSystemHelper.GetNextObject(bulletPrefab, shootingPoint.position, shootingPoint.rotation);
    }
    IEnumerator ShootRandomly()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(shotTimeMin, shotTimeMax));
            ActivateShooting();
        }
    }
    public void TakeDamage(int damage)
    {
        if (!isImmortal)
        {
            currentHealth -= damage;
        }
        if (currentHealth <= 0)
        {
            Destruction();
        }
        else
        {
            if (hitEffect)
            {
                Instantiate(hitEffect, transform.position, Quaternion.identity, transform);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            var takeDamage = (ICanTakeDamage)collision.gameObject.GetComponent(typeof(ICanTakeDamage));
            if (takeDamage != null)
            {
                takeDamage.TakeDamage(1);
            }
        }
    }
    public void Destruction()
    {
        if (destructionVFX)
            Instantiate(destructionVFX, transform.position, Quaternion.identity);
        onDeath(this);
        FollowThePath followComponent = GetComponent<FollowThePath>();
        if (followComponent != null)
        {
            Destroy(followComponent);
        }
        // xóa Component followComponent khỏi gaem object hiện tại 
        gameObject.SetActive(false);
        //Destroy(gameObject);
    }
    public IEnumerator MoveToPoint(Vector3 _target, float _delay, bool _disImmortal = false)
    {
        Vector3 startPosition = transform.position;
        float moveTime = 0;
        while (moveTime < _delay)
        {
            moveTime += Time.deltaTime;
            if(this == null)
                yield break;
            transform.position = Vector3.Lerp(startPosition, _target, moveTime / _delay);
            yield return null;
        }
        if (this != null)
            transform.position = _target;
        if (_disImmortal)
            isImmortal = false;
    }
}
