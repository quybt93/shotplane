using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public GameObject healthIcon;
    List<Image> hearthIcons = new List<Image>();


    private void Start()
    {
        hearthIcons.Add(healthIcon.GetComponent<Image>());
        for (int i = 0; i < PlayerController.instance.maxHealth; i++)
        {
            if (i > 0)
            {
                hearthIcons.Add(Instantiate(healthIcon, transform).GetComponent<Image>());
            }
            hearthIcons[i].color = Color.red;
        }

        PlayerController.healthChangeEvent += Player_healthChangeEvent;
    }

    private void Player_healthChangeEvent(int currentHealth)
    {
        for (int i = 0; i < hearthIcons.Count; i++)
        {
            hearthIcons[i].color = (i < currentHealth) ? Color.red : Color.black;
        }
    }
}
