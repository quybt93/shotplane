﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour
{
    public bool isActivated;
    public int count;
    public GameObject enemyPrefab;
    public float speed;
    public bool loop;
    public Shooting shooting;
    [HideInInspector] public List<EnemyController> enemies;
    [HideInInspector] public bool isSpawnFinished;
    [HideInInspector] public bool isWaveFinished;
    public void Awake()
    {
        enemies = new List<EnemyController>();
    }
    public virtual void OnEnable()
    {
        EnemyController.onDeath += OnEnemyDeath;
    }
    protected GameObject SpawnEnemy(Shooting shooting, Vector3 pos, bool _isImmortal)
    {
        GameObject newEnemy = SpawnSystemHelper.GetNextObject(enemyPrefab, pos, Quaternion.identity, false);
        EnemyController enemyController = newEnemy.GetComponent<EnemyController>();
        enemyController.shotTimeMin = shooting.shotTimeMin;
        enemyController.shotTimeMax = shooting.shotTimeMax;
        enemyController.isImmortal = _isImmortal;

        newEnemy.SetActive(true);

        enemies.Add(enemyController);

        return newEnemy;
    }
    protected virtual void Update()
    {
        if (enemies.Count == 0 && isSpawnFinished)
        {
            isWaveFinished = true;
            gameObject.SetActive(false);
            //Destroy(gameObject);
        }
    }
    public void OnEnemyDeath(EnemyController enemy)
    {
        if (enemies.Contains(enemy))
        {
            enemies.Remove(enemy);
        }
    }
    private void OnDisable()
    {
        EnemyController.onDeath -= OnEnemyDeath;
    }

}
